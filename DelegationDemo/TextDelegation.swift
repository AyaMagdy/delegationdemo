//
//  TextDelegation.swift
//  DelegationDemo
//
//  Created by aya magdy on 2/27/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
protocol  TextDelegation {
    func passText(inputValue : String)
}
