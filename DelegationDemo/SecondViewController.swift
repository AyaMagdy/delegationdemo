//
//  SecondViewController.swift
//  DelegationDemo
//
//  Created by aya magdy on 2/27/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController  {
   
    
    var dataPassed : String?
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var delegate :TextDelegation! // optional
    
    //var de : TextDelegation? // optional
    // ? = ! // == optional
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         label.text = dataPassed
        // Do any additional setup after loading the view/
        
    
    }
    
    @IBAction func buttonPressedToFirstController(_ sender: Any) {
        delegate?.passText(inputValue: textField.text!)
       // de!.passText(inputValue: textField.text!)
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
 
}
