//
//  ViewController.swift
//  DelegationDemo
//
//  Created by aya magdy on 2/27/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,TextDelegation{
  

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSecondScreen" {
            let destinationVc = segue.destination as! SecondViewController
            destinationVc.dataPassed = textField.text
            destinationVc.delegate = self
            
        }
    }
    
    func passText(inputValue: String) {
        label.text = inputValue
    }
    
}

